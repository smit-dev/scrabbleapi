package ee.mesh47.scrabbleapi.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.mesh47.scrabbleapi.domain.ScrabbleString;
import ee.mesh47.scrabbleapi.dto.ScrabbleRequest;
import ee.mesh47.scrabbleapi.dto.ScrabbleResponse;
import ee.mesh47.scrabbleapi.services.ScrabbleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ScrabbleController.class)
class ScrabbleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScrabbleService service;
    @Test
    void should_addWord_when_validWord() throws Exception {
        doNothing().when(service).addWord("asd");
        ScrabbleRequest request = new ScrabbleRequest();
        request.setWord("asd");
        String req = new ObjectMapper().writer().writeValueAsString(request);

        mockMvc.perform(
                post("/scrabble/words")
                        .contentType(APPLICATION_JSON)
                        .content(req))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Word is valid")));
    }

    @Test
    void should_sendErrorMessage_when_invalidWord() throws Exception {
        doNothing().when(service).addWord("asd!");
        ScrabbleRequest request = new ScrabbleRequest();
        request.setWord("asd!");
        String req = new ObjectMapper().writer().writeValueAsString(request);

        String res = new ObjectMapper().writer().writeValueAsString(
                Collections.singletonMap("word", "Invalid string, allowed characters: [a-zA-Z]"));

        mockMvc.perform(
                        post("/scrabble/words")
                                .contentType(APPLICATION_JSON)
                                .content(req))
                .andDo(print()).andExpect(status().isBadRequest())
                .andExpect(content().string(containsString(res)));
    }
    @Test
    void should_getPoints_when_validWord() throws Exception {
        ScrabbleString ss = new ScrabbleString("asd");
        when(service.calculatePoints("asd")).thenReturn(3);
        when(service.isWordInDictionary("asd")).thenReturn(true);
        ScrabbleResponse response = new ScrabbleResponse();
        response.setPoints(3);
        response.setInDictionary(true);
        String jsonResponse = new ObjectMapper().writer().writeValueAsString(response);

        mockMvc.perform(
                get("/scrabble/get-points/asd"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString(jsonResponse)));
    }
}