package ee.mesh47.scrabbleapi.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class ScrabbleServiceImplTest {

    @Test
    void should_return0calculatingPoints_when_hasInvalidInput() {
        ScrabbleServiceImpl service = new ScrabbleServiceImpl();
        Assertions.assertEquals(0, service.calculatePoints("!"));
    }

    @Test
    void should_calculatePoints_when_hasValidInput() {
        ScrabbleServiceImpl service = new ScrabbleServiceImpl();
        Assertions.assertEquals(4, service.calculatePoints("asd"));
    }
}