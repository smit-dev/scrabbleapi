package ee.mesh47.scrabbleapi.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Locale;

class ScrabbleStringTest {

    @Test
    void should_throwError_when_invalidValueIsPassed() {
        ScrabbleString ss = new ScrabbleString();
        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> ss.setValue("asd!"));

        Assertions.assertTrue(thrown.getMessage().contains("Invalid string, allowed characters: "));
    }

    @Test
    void should_calculateCorrectPointValue() throws Exception {
        ScrabbleString ss = new ScrabbleString();
        ss.setValue("abcdefghijklmnopqrstuvwxyz");
        Assertions.assertEquals(87 ,ss.getPointValue());
    }

    @Test
    void should_ignoreCase_when_calculatingCorrectPointValue() throws Exception {
        ScrabbleString ss = new ScrabbleString();
        ss.setValue("abcdefghijklmnopqrstuvwxyz".toUpperCase(Locale.ROOT));
        Assertions.assertEquals(87 ,ss.getPointValue());

        ss.setValue("abcdefghijklmnopqrstuvwxyz");
        Assertions.assertEquals(87 ,ss.getPointValue());
    }
}