package ee.mesh47.scrabbleapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class ScrabbleApiApplication {

    @Value("${api.allowed-origin}")
    private String apiAllowedOrigin;

    public static void main(String[] args) {
        SpringApplication.run(ScrabbleApiApplication.class, args);
    }
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/scrabble/**")
                        .allowedOrigins(apiAllowedOrigin);
            }
        };
    }

}
