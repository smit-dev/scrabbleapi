package ee.mesh47.scrabbleapi.repositories;

import ee.mesh47.scrabbleapi.domain.ScrabbleString;
import ee.mesh47.scrabbleapi.domain.Word;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WordRepository extends JpaRepository<Word, Long> {
    boolean existsByScrabbleString(ScrabbleString scrabbleString);
}
