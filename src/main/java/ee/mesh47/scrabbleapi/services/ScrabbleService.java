package ee.mesh47.scrabbleapi.services;

public interface ScrabbleService {
    void addWord(String word);
    boolean isWordInDictionary(String word);
    int calculatePoints(String word);
}
