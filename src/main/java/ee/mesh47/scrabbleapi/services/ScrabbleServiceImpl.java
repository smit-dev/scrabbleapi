package ee.mesh47.scrabbleapi.services;

import ee.mesh47.scrabbleapi.domain.ScrabbleString;
import ee.mesh47.scrabbleapi.domain.Word;
import ee.mesh47.scrabbleapi.repositories.WordRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ScrabbleServiceImpl implements ScrabbleService{
    @Autowired
    WordRepository wordRepository;

    @Override
    public void addWord(String word) {
        ScrabbleString ss = new ScrabbleString(word);
        if (!wordRepository.existsByScrabbleString(ss)) {
            wordRepository.save(new Word(ss));
        }
    }

    @Override
    public boolean isWordInDictionary(String word) {
        return wordRepository.existsByScrabbleString(new ScrabbleString(word));
    }

    @Override
    public int calculatePoints(String word) {
        try {
            ScrabbleString ss = new ScrabbleString(word);
            log.info(ss.getPointValueString());
            return ss.getPointValue();
        } catch (Exception e) {
            return 0;
        }
    }
}
