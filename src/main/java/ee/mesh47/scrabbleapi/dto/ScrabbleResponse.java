package ee.mesh47.scrabbleapi.dto;

import lombok.Data;

@Data
public class ScrabbleResponse {
    private int points;
    private boolean isInDictionary;
}
