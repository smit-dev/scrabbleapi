package ee.mesh47.scrabbleapi.dto;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class ScrabbleRequest {
    @Pattern(regexp = "[a-zA-Z]+", message = "Invalid string, allowed characters: [a-zA-Z]")
    private String word;
}
