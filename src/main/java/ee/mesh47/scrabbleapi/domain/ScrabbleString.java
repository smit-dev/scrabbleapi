package ee.mesh47.scrabbleapi.domain;

import ee.mesh47.scrabbleapi.exceptions.InvalidStateException;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;
import java.util.Locale;

@Embeddable
public class ScrabbleString {
    @Column(name = "scrabblestring", unique = true)
    @Pattern(regexp = "[a-zA-Z]+", message = "Invalid string, allowed characters: [a-zA-Z]")
    private String value;
    private static final String ALLOWED_CHARACTERS = "[a-zA-Z]";

    public ScrabbleString(String value){
        this.setValue(value);
    }

    public ScrabbleString() {}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        if (isValid(value)){
            this.value = value.toLowerCase(Locale.ROOT);
        } else {
            throw new IllegalArgumentException("Invalid string, allowed characters: " + ALLOWED_CHARACTERS);
        }
    }

    public int getPointValue() throws InvalidStateException {
        int pointValue;

        if (isValid()) {
            pointValue = this.value.chars().map(c -> getCharacterValue((char) c)).sum();
            return pointValue;
        }
        throw new InvalidStateException("ScrabbleString in invalid state: " + value);
    }

    public boolean isValid() {
        return isValid(this.value);
    }

    private boolean isValid(String value) {
        return value.matches(ALLOWED_CHARACTERS +"+");
    }

    private int getCharacterValue(char c) {
        return switch (c) {
            case 'e', 'a', 'i', 'o', 'n', 'r', 't', 'l', 's', 'u' -> 1;
            case 'd', 'g' -> 2;
            case 'b', 'c', 'm', 'p' -> 3;
            case 'f', 'h', 'v', 'w', 'y' -> 4;
            case 'k' -> 5;
            case 'j', 'x' -> 8;
            case 'q', 'z' -> 10;
            default -> throw new IllegalArgumentException("Invalid character: " + c);
        };
    }

    public String getPointValueString() throws InvalidStateException {
        StringBuilder equation = new StringBuilder();
        for (char c :
                this.value.toCharArray()) {
            equation.append(c)
                    .append(":")
                    .append(getCharacterValue(c))
                    .append(" ");
        }
        return equation.append("= ").append(getPointValue()).toString();
    }

    @Override
    public String toString() {
        return this.value;
    }
}
