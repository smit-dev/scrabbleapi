package ee.mesh47.scrabbleapi.domain;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "words")
public class Word {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Setter @Getter
    private Long id;

    @Embedded @Getter
    private ScrabbleString scrabbleString;

    public Word(ScrabbleString scrabbleString) {
        this.scrabbleString = scrabbleString;
    }

    public Word() {}

    public void setScrabbleString(String scrabbleString) {
        this.scrabbleString = new ScrabbleString(scrabbleString);
    }
    public void setScrabbleString(ScrabbleString scrabbleString) {
        this.scrabbleString = scrabbleString;
    }
}
