package ee.mesh47.scrabbleapi.controllers;

import ee.mesh47.scrabbleapi.dto.ScrabbleRequest;
import ee.mesh47.scrabbleapi.dto.ScrabbleResponse;
import ee.mesh47.scrabbleapi.services.ScrabbleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/scrabble")
@Slf4j
public class ScrabbleController {

    @Autowired
    private ScrabbleService scrabbleService;

    @PostMapping(path = "/words")
    public ResponseEntity<String> addWord(@Valid @RequestBody ScrabbleRequest word) {
        log.info("User adding word to dictionary: " + word);
        scrabbleService.addWord(word.getWord());
        return ResponseEntity.ok("Word is valid");
    }

    @GetMapping("/get-points/{word}")
    public ScrabbleResponse getPoints(
            @Pattern(regexp = "[a-zA-Z]+", message = "Invalid string, allowed characters: [a-zA-Z]")
            @PathVariable String word) {
        log.info("User querying points for word: " + word);

        ScrabbleResponse response = new ScrabbleResponse();
        response.setInDictionary(scrabbleService.isWordInDictionary(word));
        response.setPoints(scrabbleService.calculatePoints(word));

        return response;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}

