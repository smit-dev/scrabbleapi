package ee.mesh47.scrabbleapi.exceptions;

public class InvalidStateException extends Exception {
    public InvalidStateException(String errorMessage) {
        super(errorMessage);
    }
}