# ScrabbleAPI
API to calculate points for words and add words to scrabble dictionary.
# Getting started
Java version 17 is needed to run this project.
* clone this repo
* `./gradlew bootRun`

By default, this project runs on port 8080.

## Configuration
To allow API calls from a site set `api.allowed-origin` in application.yml file.

## Tests
To run test use `./gradlew test`